package pocket.paysolutions.controller;

import android.os.Bundle;

import pocket.paysolutions.helper.RootActivity;

/**
 * Created by Developer on 5/26/2017.
 */

public class EditBankInformationActivity extends RootActivity {

    public void presentProfileModel() {
    }

    public void bankInformationClick() {
    }

    public void save() {
    }

    public void presentActionSheet() {
    }

    public void takePicture() {
    }

    public void createFileName() {
    }

    public void copyFileToLocalDir() {
    }

    public void presentToast() {
    }

    public void pathForImage() {
    }

    public void uploadImage() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
