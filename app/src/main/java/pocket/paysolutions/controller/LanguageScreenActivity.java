package pocket.paysolutions.controller;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pocket.paysolutions.helper.RootActivity;

/**
 * Created by Developer on 5/26/2017.
 */

public class LanguageScreenActivity extends RootActivity {

    public void select(String languageCode) {
        this.languageScreenModel.setDefaultLanguage(languageCode);
        debug("select language");
        this.goLoginPage();
        this.finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.debug("language screen page");
        this.setContentView(R.layout.languagescreen_layout);
        Button english_btn = (Button) this.findViewById(R.id.english_btn);
        Button thai_btn = (Button) this.findViewById(R.id.thai_btn);
        english_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LanguageScreenActivity.this.select("en");
            }
        });
        thai_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LanguageScreenActivity.this.select("th");
            }
        });
    }
}
