package pocket.paysolutions.helper;

import java.util.ArrayList;

/**
 * Created by codel on 6/1/2017.
 */

public class Language {
    private static Language instance = null;
    public ArrayList<Listener> list = new ArrayList<Listener>();
    private SharedObject sharedObject = SharedObject.getInstance();
    public static interface Listener {
        void updateLanguage();
    }

    public Language add(Language.Listener listener){
        list.add(listener);
        return this;
    }

    public Language remove(Language.Listener listener){
        list.remove(listener);
        return this;
    }

    public Language refresh(){
        for(Listener listener:list){
            listener.updateLanguage();
        }
        return this;
    }

    public Language setLanguage(String langCode) {
        this.sharedObject.languageCode = langCode;
        this.refresh();
        return this;
    }

    public static Language getInstance() {
        if (instance == null) {
            instance = new Language();
        }
        return instance;
    }
    private Language() {
        this.setLanguage(this.sharedObject.languageCode);
    }


    public String string(String key) {
        return "";
    }

}