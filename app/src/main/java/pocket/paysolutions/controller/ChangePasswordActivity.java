package pocket.paysolutions.controller;

import android.os.Bundle;

import pocket.paysolutions.helper.Json;
import pocket.paysolutions.helper.RootActivity;

/**
 * Created by Developer on 5/26/2017.
 */

public class ChangePasswordActivity extends RootActivity {

    private String url = "";


    public void changePasswordSet(String from, String to) {
        Json json = Json.object();
        json.set("oldpassword", from);
        json.set("newpassword", to);
    }

    public void showLoading() {
    }

    public void showError() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

}
