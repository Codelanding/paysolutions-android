package pocket.paysolutions.helper.listener;

/**
 * Created by codel on 6/2/2017.
 */

public interface WebListener {
    void onComplete(String response);
}
