package pocket.paysolutions.model;

import android.content.Context;

import pocket.paysolutions.helper.Json;
import pocket.paysolutions.helper.RootModel;

/**
 * Created by Developer on 5/26/2017.
 */

public class LoginModel extends RootModel {

    public Json changePassword(String oldPassword,String newPassword){
        this.data.put("oldpassword",oldPassword);
        this.data.put("newpassword",newPassword);
        String jsonString  = this.getStringFromUrl("",this.data);
        return Json.read(jsonString);
    }

    public Json login(String account, String password) {
        // Making HTTP request
        Json result = Json.object();
        this.data.put("account",account);
        this.data.put("password",password);
        String jsonString = this.getStringFromUrl("",this.data);
        return Json.read(jsonString);
    }

    public LoginModel(Context context) {
        super(context);
    }
}
