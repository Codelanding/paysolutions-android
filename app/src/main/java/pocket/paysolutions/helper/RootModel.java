package pocket.paysolutions.helper;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import pocket.paysolutions.helper.sqlite.DBHelper;
import pocket.paysolutions.helper.sqlite.SQLite;

/**
 * Created by codel on 6/1/2017.
 */

public abstract class RootModel {

    public SharedObject sharedObject = SharedObject.getInstance();
    public HashMap<String, Object> data = new HashMap<String, Object>();
    //public String section = getSection();
    //public abstract String getSection();
    public String url = "";
    protected SQLite sqlite = null;
    private Context context = null;
    private DBHelper helper = null;
    private String api_key = "wbR+r8kgZOg4QGX7qPxsug==";

    public RootModel(Context context) {
        this.context = context;
        this.helper = new DBHelper(context);
        //this.sqlite = this.helper.getWritableDatabase();
    }

    //
    private String getPostDataString(HashMap<String, Object> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
        }

        return result.toString();
    }

    //
    public String getStringFromUrl(String section,HashMap<String, Object> data) {
        URL url;
        String response = "";
        data.put("api_key", this.api_key);
        try {
            url = new URL(this.url + section);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(data));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
}
