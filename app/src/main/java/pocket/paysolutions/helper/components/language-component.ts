/*
import { Injectable, Component } from '@angular/core';
*/
import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { TranslateService } from 'ng2-translate';
import { StorageService } from '../providers/storage-service';

@Component({
  template: `
    <ion-list>
      <ion-list-header>{{"language.title" | translate}}</ion-list-header>
      <button ion-item (click)="chooseLanguage('en')">
        <ion-avatar item-left>
          <img src="assets/flag/usd.jpg">
        </ion-avatar>
        <h2>{{"language.english" | translate}}</h2>
      </button>

      <button ion-item (click)="chooseLanguage('th')">
        <ion-avatar item-left>
          <img src="assets/flag/thai.jpg">
        </ion-avatar>
        <h2>{{"language.thai" | translate}}</h2>
      </button>

    </ion-list>
  `
})

export class LanguageComponent {

  constructor(public viewCtrl: ViewController, private translate: TranslateService, public storageServ:StorageService) {
  }

  public showPopupLanguage() {
    console.log('showPopupLanguage');
  }

  public chooseLanguage(value){
    this.translate.setDefaultLang(value);
    this.translate.use(value);
    this.storageServ.write('language', value);
    console.log('chooseLanguage ', value);
    console.log("PAGE Component SELECT=>"+this.storageServ.read<string>('language'));
    this.close();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
