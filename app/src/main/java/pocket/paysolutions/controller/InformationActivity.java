package pocket.paysolutions.controller;

import android.os.Bundle;

import pocket.paysolutions.helper.RootActivity;

/**
 * Created by Developer on 5/26/2017.
 */

public class InformationActivity extends RootActivity {

    public void loadProfileInfo() {
    }

    public void itemSelected() {
    }

    public void editInformation() {
    }

    public void logout() {
    }

    public void bankInformation() {
    }

    public void changePassword() {
    }

    public void tax() {
    }

    public void invite() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
