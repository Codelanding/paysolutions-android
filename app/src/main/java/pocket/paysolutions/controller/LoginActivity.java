package pocket.paysolutions.controller;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pocket.paysolutions.helper.Json;
import pocket.paysolutions.helper.RootActivity;

/**
 * Created by Developer on 5/26/2017.
 */

public class LoginActivity extends RootActivity {


    public String pageUrl() {
        return "account/login";
    }

    public void createAccount() {
    }

    public void doLogin() {
        //this.goHome();
        this.goApplication();
    }

    public void forgotPassword() {

    }

    public void changePasswordSet(String from, String to) {
        Json json = Json.object();
        json.set("oldpassword", from);
        json.set("newpassword", to);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.login_layout);
        Button login_btn = (Button) this.findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.doLogin();
            }
        });
        Button register_btn = (Button)this.findViewById(R.id.register_btn);
        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.goRegister();
            }
        });
        Button forgotpassword_btn = (Button)this.findViewById(R.id.forgotpassword_btn);
        forgotpassword_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.goForgotPassword();
            }
        });
    }
}
