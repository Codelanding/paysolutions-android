package pocket.paysolutions.controller;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import pocket.paysolutions.helper.RootActivity;

/**
 * Created by Developer on 5/26/2017.
 */

public class HomeActivity extends RootActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.home_page);
        debug("HomeActivity");
        ImageView payment = (ImageView)this.findViewById(R.id.request_payment_btn);
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                debug("payment clicked");
                HomeActivity.this.openPaymentPage();
            }
        });

        ImageView send_payment = (ImageView)this.findViewById(R.id.send_payment_btn);
        send_payment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                debug("send clicked");
                return false;
            }
        });

    }
}
