package pocket.paysolutions.controller;

import android.os.Bundle;

import pocket.paysolutions.helper.RootActivity;

public class PreloadActivity extends RootActivity    {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.preload_layout);

        Thread timer = new Thread() {
            public void run(){
                try {
                    sleep(5000);
                    PreloadActivity.this.goLanguagePage();
                    PreloadActivity.this.finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        };
        timer.start();
    }
}
