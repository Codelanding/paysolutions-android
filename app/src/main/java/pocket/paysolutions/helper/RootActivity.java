package pocket.paysolutions.helper;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import pocket.paysolutions.controller.ApplicationActivity;
import pocket.paysolutions.controller.ContactUsActivity;
import pocket.paysolutions.controller.ForgotActivity;
import pocket.paysolutions.controller.HomeActivity;
import pocket.paysolutions.controller.LanguageScreenActivity;
import pocket.paysolutions.controller.LoginActivity;
import pocket.paysolutions.controller.PaymentActivity;
import pocket.paysolutions.controller.RegisterActivity;
import pocket.paysolutions.controller.RequestPaymentMoneyActivity;
import pocket.paysolutions.model.BankInformationModel;
import pocket.paysolutions.model.ChangePasswordModel;
import pocket.paysolutions.model.ContactUsModel;
import pocket.paysolutions.model.CopyInviteModel;
import pocket.paysolutions.model.CurrencyModel;
import pocket.paysolutions.model.EditBankInformationModel;
import pocket.paysolutions.model.EditProfileModel;
import pocket.paysolutions.model.EditTaxModel;
import pocket.paysolutions.model.ForgotModel;
import pocket.paysolutions.model.FriendGetFriendModel;
import pocket.paysolutions.model.HelpFaqDetailModel;
import pocket.paysolutions.model.HelpFaqModel;
import pocket.paysolutions.model.HelpTermsModel;
import pocket.paysolutions.model.HomeModel;
import pocket.paysolutions.model.InformationModel;
import pocket.paysolutions.model.InviteModel;
import pocket.paysolutions.model.LanguageScreenModel;
import pocket.paysolutions.model.LoginModel;
import pocket.paysolutions.model.MenuPaymentModel;
import pocket.paysolutions.model.NotificationModel;
import pocket.paysolutions.model.PaymentModel;
import pocket.paysolutions.model.PreloadModel;
import pocket.paysolutions.model.ProfileModel;
import pocket.paysolutions.model.PurchaseDetailModel;
import pocket.paysolutions.model.PurchaseDetailSearchModel;
import pocket.paysolutions.model.PurchaseModel;
import pocket.paysolutions.model.RegisterModel;
import pocket.paysolutions.model.RequestConfirmModel;
import pocket.paysolutions.model.RequestPaymentMoneyModel;
import pocket.paysolutions.model.RequestQrcodeModel;
import pocket.paysolutions.model.RevenueDetailModel;
import pocket.paysolutions.model.RevenueModel;
import pocket.paysolutions.model.SettingsModel;
import pocket.paysolutions.model.SystemModel;
import pocket.paysolutions.model.TabsModel;
import pocket.paysolutions.model.TaxModel;
import pocket.paysolutions.model.TermsModel;

/**
 * Created by Developer on 5/26/2017.
 */

public abstract class RootActivity extends AppCompatActivity implements Language.Listener {
    //
    public SharedObject sharedObject = SharedObject.getInstance();
    public Language language = Language.getInstance();
    public boolean DEBUG = true;
    //

    //
    public BankInformationModel bankInformationModel;
    public ChangePasswordModel changePasswordModel;
    public ContactUsModel contactUsModel;
    public CopyInviteModel copyInviteModel;
    public CurrencyModel currencyModel;
    public EditBankInformationModel editBankInformationModel;
    public EditProfileModel editProfileModel;
    public EditTaxModel editTaxModel;
    public ForgotModel forgotModel;
    public FriendGetFriendModel friendModel;
    public HelpFaqDetailModel helpFaqDetailModel;
    public HelpFaqModel helpFaqModel;
    public HelpTermsModel helpTermsModel;
    public HomeModel homeModel;
    public InformationModel informationModel;
    public InviteModel inviteModel;
    public LanguageScreenModel languageScreenModel;
    public LoginModel loginModel;
    public MenuPaymentModel menuPaymentModel;
    public NotificationModel notificationModel;
    public PaymentModel paymentModel;
    public PreloadModel preloadModel;
    public ProfileModel profileModel;
    public PurchaseModel purchaseModel;
    public PurchaseDetailModel purchaseDetailModel;
    public PurchaseDetailSearchModel purchaseDetailSearchModel;
    public RegisterModel registerModel;
    public RequestConfirmModel requestConfirmModel;
    public RequestPaymentMoneyModel requestPaymentMoneyModel;
    public RequestQrcodeModel requestQrcodeModel;
    public RevenueModel revenueModel;
    public RevenueDetailModel revenueDetailModel;
    public SettingsModel settingsModel;
    public SystemModel systemModel;
    public TabsModel tabsModel;
    public TaxModel taxModel;
    public TermsModel termsModel;

    //
    public HashMap<String, View> components = new HashMap<String, View>();


    public String assets(String path) {
        StringBuilder buf = new StringBuilder();
        BufferedReader in = null;

        try {
            InputStream json = this.getAssets().open(path);
            in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;

            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
        } catch (Exception e) {

        }
        try {
            if (in != null)
                in.close();
        } catch (IOException ioe) {

        }
        return buf.toString();
    }

    @Override
    public void updateLanguage() {
        for (Map.Entry<String, View> entry : components.entrySet()) {
            String key = entry.getKey();
            View view = entry.getValue();
        }
    }

    public void languagePopover() {

    }

    public void languageSelect() {
    }

    public void showPreload() {
    }

    public void showError() {
    }

    public void showNotification() {
    }

    public void saveQrCode() {
    }

    public Bitmap get(String imagepath) {
        return null;
    }

    public void debug(String message) {
        if (DEBUG)
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.language.remove(this);
    }

    public void goApplication(){
        Intent intent = new Intent(this, ApplicationActivity.class);
        startActivity(intent);
    }

    public void goLoginPage() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void goLanguagePage() {
        Intent intent = new Intent(this, LanguageScreenActivity.class);
        startActivity(intent);
    }

    public void goHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    public void goRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
    public  void goForgotPassword(){
        Intent intent = new Intent(this, ForgotActivity.class);
        startActivity(intent);

    }

    public void openPaymentPage(){
        Intent intent = new Intent(this, PaymentActivity.class);
        startActivity(intent);
    }

    public void openSendPayment(){
        Intent intent = new Intent(this, RequestPaymentMoneyActivity.class);
        startActivity(intent);
    }

    public void goContactUsPage() {
        Intent intent = new Intent(this, ContactUsActivity.class);
        startActivity(intent);
    }

//    @Override
//    public void onBackPressed() {
//        new AlertDialog.Builder(this)
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .setTitle("Closing Activity")
//                .setMessage("Are you sure you want to close this activity?")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        finish();
//                    }
//                })
//                .setNegativeButton("No", null)
//                .show();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.language.setLanguage(this.sharedObject.languageCode);
        this.language.add(this);
        // this.setContentView(R.layout.preload_layout);

        this.bankInformationModel = new BankInformationModel(this);
        this.changePasswordModel = new ChangePasswordModel(this);
        this.contactUsModel = new ContactUsModel(this);
        this.copyInviteModel = new CopyInviteModel(this);
        this.currencyModel = new CurrencyModel(this);
        this.editBankInformationModel = new EditBankInformationModel(this);
        this.editProfileModel = new EditProfileModel(this);
        this.editTaxModel = new EditTaxModel(this);
        this.forgotModel = new ForgotModel(this);
        this.friendModel = new FriendGetFriendModel(this);
        this.helpFaqDetailModel = new HelpFaqDetailModel(this);
        this.helpFaqModel = new HelpFaqModel(this);
        this.helpTermsModel = new HelpTermsModel(this);
        this.homeModel = new HomeModel(this);
        this.informationModel = new InformationModel(this);
        this.inviteModel = new InviteModel(this);
        this.languageScreenModel = new LanguageScreenModel(this);
        this.loginModel = new LoginModel(this);
        this.menuPaymentModel = new MenuPaymentModel(this);
        this.notificationModel = new NotificationModel(this);
        this.paymentModel = new PaymentModel(this);
        this.preloadModel = new PreloadModel(this);
        this.profileModel = new ProfileModel(this);
        this.purchaseModel = new PurchaseModel(this);
        this.purchaseDetailModel = new PurchaseDetailModel(this);
        this.purchaseDetailSearchModel = new PurchaseDetailSearchModel(this);
        this.registerModel = new RegisterModel(this);
        this.requestConfirmModel = new RequestConfirmModel(this);
        this.requestPaymentMoneyModel = new RequestPaymentMoneyModel(this);
        this.requestQrcodeModel = new RequestQrcodeModel(this);
        this.revenueModel = new RevenueModel(this);
        this.revenueDetailModel = new RevenueDetailModel(this);
        this.settingsModel = new SettingsModel(this);
        this.systemModel = new SystemModel(this);
        this.tabsModel = new TabsModel(this);
        this.taxModel = new TaxModel(this);
        this.termsModel = new TermsModel(this);
        //
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.hide();
        //
    }
}
