package pocket.paysolutions.helper;

import java.util.HashMap;

/**
 * Created by Developer on 5/26/2017.
 */

public class SharedObject {
    private static SharedObject instance = null;
    public String languageCode = "en";
    public HashMap<String,Object> data = new HashMap<String,Object>();
    public static SharedObject getInstance(){
        if(instance==null){
            instance = new SharedObject();
        }
        return instance;
    }
    private SharedObject(){}
}
