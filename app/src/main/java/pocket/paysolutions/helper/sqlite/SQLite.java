package pocket.paysolutions.helper.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codel on 6/2/2017.
 */

public class SQLite   {

    private Context context = null;
    private SQLiteDatabase database = null;
    private DBHelper dbHelper = null;
    private HashMap<String,Object> params = null;
    private HashMap<String,Object> where = null;
    private String sqliteFile = null;
    private String table = null;


    public String compileCommand(){

        return "";
    }

    // maxx : return first row
    public HashMap<String,Object> getRow()throws Exception{

        try {
            ArrayList<HashMap<String, Object>> result = this.getRows();
            return (result.size() > 0)?result.get(0):null;
        }catch (Exception e){
            throw e;
        }
    }
    // maxx : return rows
    public ArrayList<HashMap<String, Object>> getRows()throws  Exception{
        if(this.table==null){
            throw new Exception("please select table first");
        }
        String sql = this.compileCommand();
        this.dbHelper = new DBHelper(this.context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();



        if(db!=null)
            db.close();
        return null;
    }
    // maxx : use for update table
    public void execute(){

    }

    public SQLite where(String key,Object value){
        this.params.put(key,value);
        return this;
    }
    public ArrayList<HashMap<String, String>> query(){
        this.query(this.compileCommand());
        return null;
    }
    public ArrayList<HashMap<String, String>> query(String sql){

        return null;
    }
    public SQLite execute(String dbname){
    return this;
    }

    public void write(String table){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (Map.Entry<String,Object> entry: this.params.entrySet()){
            String key = entry.getKey();
            Object value = entry.getValue();
            if(value instanceof String){
                values.put(key,value.toString());
            }else if(value instanceof  Integer){
                try {
                    values.put(key, Integer.valueOf(value.toString()));
                }catch (Exception e){}
            }
        }
        // It's a good practice to use parameter ?, instead of concatenate string
        //db.update(table, values, Student.KEY_ID + "= ?", new String[] { String.valueOf(student.student_ID) });
        db.close(); // Closing database connection
    }
    public ArrayList<HashMap<String, String>> read(String sql) {
        //Open connection to read only
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<HashMap<String, String>> output = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(sql, null);
        int colCount = cursor.getColumnCount();
        int rowCount = cursor.getCount();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> col = new HashMap<String, String>();
                int size =cursor.getColumnCount();
                for (int i = 0; i < size; i++) {
                    col.put(cursor.getColumnName(i), cursor.getString(i));
                }
                output.add(col);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        if(db!=null)
            db.close();

        return output;

    }

    public SQLite(Context context,String sqliteFile){
        this.context = context;
        this.sqliteFile = sqliteFile;
    }
}
